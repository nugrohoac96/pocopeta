﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web;
using System.Web.UI.WebControls;
using System.Security.Cryptography;
using System.Configuration;

namespace NORTHWIND.Common
{
    public class Commons
    {
        public static string DEFAULT_FORMAT_DATE = "dd-MMM-yyyy";

        public static int GetInt(object obj)
        {
            try
            {
                return Convert.ToInt32(obj);
            }
            catch (Exception ex) { }
            return 0;
        }

        public static DateTime GetDateTime(object obj)
        {
            try
            {
                return Convert.ToDateTime(obj);
            }
            catch (Exception ex) { }
            return DateTime.Now;
        }

        public static double GetDouble(object obj)
        {
            try
            {
                return Convert.ToDouble(obj);
            }
            catch (Exception ex) { }
            return 0;
        }

        public static decimal GetDecimal(object obj)
        {
            try
            {
                return Convert.ToDecimal(obj);
            }
            catch (Exception ex) { }
            return 0;
        }

        public static string GetCurrentLoginUser()
        {
            //HttpContext.Current.Session["UserId"] = @"AHZA-INTEGRASI/MNUR";
            if (HttpContext.Current.Session["USER_ID"] != null)
                return HttpContext.Current.Session["USER_ID"].ToString();
            else
                return null;

        }

        public static string GetConfigValue(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        public static string GetCurrentLoginUserName()
        { 
            if (HttpContext.Current.Session["USER_NAME"] != null)
                return HttpContext.Current.Session["USER_NAME"].ToString();
            else
                return null;

        }

        public static void PopulateDropDownValue(DropDownList dropDownList, DataTable dataSource, string dataValueField, string dataTextField)
        {
            dropDownList.DataValueField = dataValueField;
            dropDownList.DataTextField = dataTextField;
            dropDownList.DataSource = dataSource;
            dropDownList.DataBind();
            dropDownList.Items.Insert(0, new ListItem("", ""));
            if (dataSource.Rows.Count > 0)
                dropDownList.SelectedIndex = 0;

        }

        public static string GetImageUrl(byte[] bytes)
        {
            //Cuma bisa untuk image di NorthWind
            if (bytes.Length >=78)
            {
                return "data:image/png;base64," + Convert.ToBase64String(bytes, 78, bytes.Length - 78);
            }

            return "";
        }

        public static string GetMd5Hash(MD5 md5Hash, string input)
        {

            // Convert the input string to a byte array and compute the hash.
            byte[] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(input));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString().ToUpper();
        }

    }
}
